<?php
namespace LibraryApi\Tests;

use Faker\Factory as Faker;
use LibraryApi\Helpers\Factory;
use LibraryApi\BookLibrary;

use LibraryApi\HttpClients\HttpClient;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;


class BookLibraryTest extends TestCase
{
    /**
     * @var HttpClient
     */
    private $httpClient;

    /**
     * @var BookLibrary
     */
    private $library;


    private $params;


    protected function setUp()
    {
        parent::setUp();

        $this->httpClient = $this->prophesize(HttpClient::class);
        Factory::bind(HttpClient::class, $this->httpClient->reveal());

        $this->params =[
            'limit' => Faker::create()->numberBetween(1,10),
            'offset' => Faker::create()->numberBetween(1,10),
        ];

        $this->library  = new BookLibrary();
    }

    public function testShouldGetBooks()
    {
        $this->httpClient->sendRequest(Argument::containingString('/books'), $this->params)
            ->shouldBeCalled()
            ->willReturn([]);

        $this->assertEquals(
            [],
            $this->library->getBooks($this->params['limit'], $this->params['offset'])
        );
    }

    public function testShouldGetAuthors()
    {
        $this->httpClient->sendRequest(Argument::containingString('/authors'), $this->params)
            ->shouldBeCalled()
            ->willReturn([]);

        $this->assertEquals(
            [],
            $this->library->getAuthors($this->params['limit'], $this->params['offset'])
        );
    }

    public function testShouldGetAuthorBooks()
    {
        $authorId =  Faker::create()->numberBetween(1,10);

        $this->httpClient->sendRequest(Argument::containingString("/authors/{$authorId}/books"), $this->params)
            ->shouldBeCalled()
            ->willReturn([]);

        $this->assertEquals(
            [],
            $this->library->getAuthorBooks($authorId, $this->params['limit'], $this->params['offset'])
        );
    }
}