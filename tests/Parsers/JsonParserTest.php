<?php
namespace LibraryApi\Tests\Parsers;

use LibraryApi\Exceptions\InvalidDataFormatException;
use LibraryApi\Parsers\JsonParser;
use PHPUnit\Framework\TestCase;


class JsonParserTest extends TestCase
{
    /**
     * @var JsonParser
     */
    private $parser;

    protected function setUp()
    {
        parent::setUp();
        $this->parser = new JsonParser();
    }

    public function testShouldParseValidJsonString()
    {
        $data = '{"info": ["author", "book"] }';

        $this->assertEquals(
            json_decode($data,true),
            $this->parser->parse($data)
        );
    }

    public function testShouldThrowInvalidDataFormatExceptionOnInvalidData()
    {
        $data = '{ invalid JSON }';
        $this->expectException(InvalidDataFormatException::class);
        $this->parser->parse($data);
    }

}