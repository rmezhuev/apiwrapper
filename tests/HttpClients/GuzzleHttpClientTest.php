<?php
namespace LibraryApi\Tests\HttpClients;

use GuzzleHttp\Client;
use LibraryApi\Exceptions\InvalidRequestException;
use LibraryApi\Exceptions\InvalidResponseException;
use LibraryApi\Exceptions\NotFoundException;
use LibraryApi\Exceptions\ServerConnectionException;
use LibraryApi\Helpers\Factory;
use LibraryApi\HttpClients\GuzzleHttpClient;
use LibraryApi\Parsers\JsonParser;
use LibraryApi\Parsers\Parser;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Psr\Http\Message\ResponseInterface;


class GuzzleHttpClientTest extends TestCase
{
    /**
     * @var GuzzleHttpClient
     */
    private $httpClient;

    /**
     * @var Client
     */
    private $guzzle;

    /**
     * @var ResponseInterface
     */
    private $response;

    private $uri = '/books';
    private $method = 'GET';

    protected function setUp()
    {
        parent::setUp();
        Factory::assign(Parser::class, JsonParser::class);

        $this->setupMocks();
        $this->declareBaseExpectations();

        $this->httpClient = new GuzzleHttpClient();
    }

    public function testShouldThrowInvalidRequestExceptionOnInvalidRequest()
    {
        $this->expectException(InvalidRequestException::class);

        $invalidRequestResponse = '{"status":"INVALID_REQUEST","message":"Invalid Request"}';
        $this->response->getBody()->willReturn($invalidRequestResponse);

        $this->httpClient->sendRequest($this->uri, []);
    }

    public function testShouldThrowNotFoundExceptionOnNotFoundRequest()
    {
        $this->expectException(NotFoundException::class);

        $notFoundRequestResponse = '{"status":"NOT_FOUND","message":"Not found Request"}';
        $this->response->getBody()->willReturn($notFoundRequestResponse);

        $this->httpClient->sendRequest($this->uri, []);
    }

    public function testShouldThrowInvalidResponseExceptionInvalidResponse()
    {
        $this->expectException(InvalidResponseException::class);

        $invalidResponse = '{"status":"WRONG_STATUS","message":"Something went really wrong"}';
        $this->response->getBody()->willReturn($invalidResponse);

        $this->httpClient->sendRequest($this->uri, []);
    }

    public  function testShouldThrowServerConnectionExceptionOnFailedRequest()
    {
        $this->expectException(ServerConnectionException::class);
        $this->response->getStatusCode()->willReturn(500);
        $this->httpClient->sendRequest($this->uri, []);
    }

    public function testShouldReturnDataOnSuccessResponse()
    {
        $successResponse = '{"status":"OK","message":"Ok","data":{"some": "info"},"limit":0,"offset":0 }';
        $this->response->getBody()->willReturn($successResponse);

        $data = $this->httpClient->sendRequest($this->uri, []);

        foreach (['data', 'limit', 'offset'] as $key){
            $this->assertArrayHasKey($key, $data);
        }

        foreach ( ['status', 'message'] as $key){
            $this->assertArrayNotHasKey($key, $data);
        }
    }



    private function setupMocks()
    {
        $this->guzzle = $this->prophesize(Client::class);
        Factory::bind(Client::class, $this->guzzle->reveal());

        $this->response = $this->prophesize(ResponseInterface::class);
    }

    private function declareBaseExpectations()
    {
        $this->guzzle->request($this->method, Argument::containingString($this->uri))
            ->shouldBeCalled()
            ->willReturn($this->response->reveal());

        $this->response->getStatusCode()->willReturn(200);
    }



}