<?php
use LibraryApi\Helpers\Factory;
use LibraryApi\HttpClients\GuzzleHttpClient;
use LibraryApi\HttpClients\HttpClient;
use LibraryApi\Parsers\JsonParser;
use LibraryApi\Parsers\Parser;

\ConfigLoader::loadDefault();

Factory::assign(Parser::class, JsonParser::class);
Factory::assign(HttpClient::class, GuzzleHttpClient::class);

//for testing purposes
Factory::assign(GuzzleHttp\Client::class, GuzzleHttp\Client::class);