<?php

class ConfigLoader
{
    const LOCAL_CONFIG_PATH = __DIR__.'/./config.php';

    private static $config = [];

    public static function loadDefault()
    {
        if(!self::$config){
            self::$config = require(self::LOCAL_CONFIG_PATH);
        }
    }

    public static function get(): array
    {
        self::loadDefault();
        return self::$config;
    }

    public static function set(array $overwrite)
    {
        self::$config = array_merge($overwrite);
    }

}