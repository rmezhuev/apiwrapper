<?php

namespace LibraryApi\Helpers;
use ReflectionClass;

class Factory
{
    private static $assigns = [];
    private static $bindings = [];

    public static function assign(string $baseType, string $implementation)
    {
        $reflection = new ReflectionClass($implementation); //throws exception on wrong type
        if(!$reflection->isInstantiable()){
            throw new \Exception('Given implementation is not instantiable');
        }
        self::$assigns[$baseType] = $implementation;
    }

    public static function bind(string $baseType, $object)
    {

        if(!is_object($object))
        {
            throw new \Exception('Binding should be an object');
        }
        self::$bindings[$baseType] = $object;
    }


    public static function make(string $type)
    {
        $instance = self::getBinding($type) ?? self::getAssigned($type);

        if (is_null($instance)) {
            throw new \Exception("Unknown type:'$type', please use assign or bind first");
        }

        return $instance;
    }

    public static function getAssignedClass(string $key)
    {
        if(array_key_exists($key, self::$assigns)){
            return self::$assigns[$key];
        }
        throw new \Exception("Unknown type:'$key', please use assign or bind first");
    }

    private static function getBinding(string $key)
    {
        if(array_key_exists($key, self::$bindings)){
            return self::$bindings[$key];
        }
        return null;
    }

    private static function getAssigned(string $key)
    {
        if(array_key_exists($key, self::$assigns)){
            return new self::$assigns[$key]();
        }
        return null;
    }

}