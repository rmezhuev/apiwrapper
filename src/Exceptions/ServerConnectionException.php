<?php

namespace LibraryApi\Exceptions;

/**
 * Class ServerConnectionException
 *
 * @package \LibraryApi\Exceptions
 */
class ServerConnectionException extends \Exception
{

}