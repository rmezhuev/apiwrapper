<?php

namespace LibraryApi\Exceptions;

/**
 * Class InvalidResponseException
 *
 * @package \LibraryApi\Exceptions
 */
class InvalidResponseException extends \Exception
{

}