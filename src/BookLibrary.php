<?php

namespace LibraryApi;

use LibraryApi\Helpers\Factory;
use LibraryApi\HttpClients\HttpClient;

require_once __DIR__.'/../bootstrap/bootstrap.php';

class BookLibrary
{
    /**
     * @var HttpClient
     */
    private $httpClient;


    public function __construct(array $config = [])
    {
        $this->httpClient = Factory::make(HttpClient::class);
        $config && \ConfigLoader::set($config);
    }

    public function getBooks(int $limit = 0, int $offset = 0): array
    {
        return $this->httpClient->sendRequest(
            '/books',
            ['limit' => $limit, 'offset' => $offset]
        );
    }

    public function getAuthors(int $limit = 0, int $offset = 0): array
    {
        return $this->httpClient->sendRequest(
            '/authors',
            ['limit' => $limit, 'offset' => $offset]
        );
    }

    public function getAuthorBooks(string $authorId, int $limit = 0, int $offset = 0): array
    {
        return $this->httpClient->sendRequest(
            "/authors/{$authorId}/books",
            ['limit' => $limit, 'offset' => $offset]
        );
    }


}