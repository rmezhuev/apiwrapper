<?php

namespace LibraryApi\Parsers;

use LibraryApi\Exceptions\InvalidDataFormatException;

class JsonParser implements Parser
{
    public function parse( string $data ): array
    {
        $parsedData = json_decode($data, true);

        if($parsedData === NULL){
            throw new InvalidDataFormatException();
        }

        return $parsedData;
    }
}