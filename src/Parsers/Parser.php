<?php

namespace LibraryApi\Parsers;

interface Parser
{
    public function parse(string $data): array;

}