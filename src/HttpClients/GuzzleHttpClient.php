<?php

namespace LibraryApi\HttpClients;

use GuzzleHttp\Client;
use LibraryApi\Exceptions\InvalidRequestException;
use LibraryApi\Exceptions\InvalidResponseException;
use LibraryApi\Exceptions\NotFoundException;
use LibraryApi\Exceptions\ServerConnectionException;
use LibraryApi\Helpers\Factory;
use LibraryApi\Parsers\Parser;
use Psr\Http\Message\ResponseInterface;

/**
 * Class GuzzleHttpClient
 *
 * @package \LibraryApi\HttpClients
 */
class GuzzleHttpClient extends HttpClient
{
    /**
     * @var Client
     */
    private $guzzleHttp;


    public function __construct()
    {
        $this->responseParser = Factory::make(Parser::class);
        $this->guzzleHttp = Factory::make(Client::class);
        $this->serverUri = $this->getServerUri();
    }

    public function sendRequest(string $resource, array $params = [], string $method = 'GET'): array
    {
        $uriParams = $this->constructRequestUriParams($params);

        $response = $this->guzzleHttp->request($method, "{$this->serverUri}{$resource}{$uriParams}");
        $this->assertSuccessfulRequestToServer($response);

        $content = $this->responseParser->parse($response->getBody());
        $this->assertSuccessfulApiRequest($content);

        return $this->removeServiceResponseInfo($content);
    }

    private function assertSuccessfulRequestToServer(ResponseInterface $response)
    {
        $code = $response->getStatusCode();
        if($code !== 200){
            throw new ServerConnectionException("Unable to send request to the server, status code is: $code");
        }
    }

    private function assertSuccessfulApiRequest(array $response)
    {
        if(@$response['status'] === 'INVALID_REQUEST'){
            throw new InvalidRequestException(@$response['message']);
        }

        if(@$response['status'] === 'NOT_FOUND'){
            throw new NotFoundException(@$response['message']);
        }

        if(@$response['status'] !== 'OK'){
            throw new InvalidResponseException('Invalid response from server');
        }
    }
}