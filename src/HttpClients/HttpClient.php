<?php

namespace LibraryApi\HttpClients;

use LibraryApi\Parsers\Parser;

abstract class HttpClient
{
    protected $responseServiceKeys = ['status', 'message'];

    protected $serverUri;

    /**
     * @var Parser
     */
    protected $responseParser;

    public abstract function sendRequest(string $resource, array $params = [], string $method = 'GET'): array;

    protected function constructRequestUriParams(array $params): string
    {
        $uriParams = '';

        if($params){
            $uriParams .= '?'.array_keys($params)[0].'='.array_shift($params);
        }

        foreach ($params as $key => $value){
            $uriParams .= "&{$key}={$value}";
        }

        return $uriParams;
    }

    protected function removeServiceResponseInfo(array $collection)
    {
        foreach ($this->responseServiceKeys as $key){
            unset($collection[$key]);
        }

        return $collection;
    }

    protected function getServerUri()
    {
        $config = \ConfigLoader::get();
        return "{$config['protocol']}://{$config['host']}:{$config['port']}";
    }
}